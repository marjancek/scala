class Accumulate {
  /**
   * Keep your hands off for comprehension functionality and any methods that accept a function as a parameter
   *  (map, flatMap, fold, etc.) provided by the standard library!
   */
  def accumulate[A, B](f: (A) => B, list : List[A]): List[B] = list match {
    case Nil => Nil
    case h::tail => f(h) :: accumulate(f, tail)
  }
  
}
