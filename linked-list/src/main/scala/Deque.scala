case class Deque[T]() {
  var head: Option[Node[T]] = None
  var toe: Option[Node[T]] = None

  def push(value: T): Unit = toe match {
    case None => {
      toe = Some(Node(value, None, None))
      head = toe
    }
    case Some(node) => {
      node.front = Some(Node(value, None, Some(node)))
      toe = node.front
    }
  }
  
  def pop: Option[T] = toe match {
    case None => None
    case Some(node) => {
      toe = node.back
      if (toe == None) head = None
      Some(node.value)
    }
  }
 
  def unshift(value: T): Unit =head match {
    case None => {
      head = Some(Node(value, None, None))
      toe = head
    }
    case Some(node) => {
      node.back = Some(Node(value, Some(node), None))
      head = node.back
    }
  }  

  def shift: Option[T] =    head match {
    case None => None
    case Some(node) => {
      head = node.front
      if (head == None) toe = None
      Some(node.value)
    }
  }

  
  case class Node[T](var value: T, var front: Option[Node[T]], var back: Option[Node[T]])
}