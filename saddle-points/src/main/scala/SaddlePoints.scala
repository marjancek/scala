case class Matrix(m: List[List[Int]]) {
  lazy val t = m.transpose
  lazy val maxs = m.map(_.max)
  lazy val mins = t.map(_.min)

  def saddlePoints: Set[(Int, Int)] = if (m.length == 0 || t.length == 0) Set()
  else (for {
    (maxRow, x) <- maxs.zipWithIndex
    (minCol, y) <- mins.zipWithIndex
    if (maxRow == minCol)
  } yield (x, y)).toSet

}