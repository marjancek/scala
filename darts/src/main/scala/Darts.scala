object Darts {

  def score(x: Double, y: Double): Int = {
    val d = x * x + y * y
    if (d <= 1) 10
    else if (d <= 25) 5
    else if (d <= 100) 1
    else 0
  }

}