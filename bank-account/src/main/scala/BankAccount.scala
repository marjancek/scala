import java.util.concurrent.atomic.AtomicInteger

trait BankAccount {

  def closeAccount(): Unit

  def getBalance: Option[Int]

  def incrementBalance(increment: Int): Option[Int]
}

class Account extends BankAccount {
  var ref: Option[AtomicInteger] = Some(new AtomicInteger(0))

  def closeAccount(): Unit = ref = None

  def getBalance: Option[Int] = ref.map(_.get)

  def incrementBalance(increment: Int): Option[Int] = ref.map(_.addAndGet(increment))
}

object Bank {
  def openAccount(): BankAccount = new Account
}

