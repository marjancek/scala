case class Bowling(val points: Option[Int] = Some(0), val frame: Int = 1, val ball: Int = 1, val fshot: Int = 0, val pstrike: Int = 0, val pspare: Int = 0) {

  def score(): Either[Unit, Int] = (validScore, points) match {
    case (true, Some(p)) => Right(p)
    case _ => Left()
  }

  def validScore(): Boolean = (frame == 10 && ball == 2 && (pstrike + pspare) == 0) || (frame == 13)

  def validRoll(down: Int): Boolean = (down >= 0) && (down <= 10) && (down + fshot <= 10) && (frame <= 12)

  def roll(down: Int): Bowling = if (!validRoll(down)) Bowling(None)
  else {
    val strikes = if (down == 10) 1 else 0
    // last frame strikes count as this frame spares (count one more shot)
    val spares = pstrike + (if (down < 10 && fshot < 10 && down + fshot == 10) 1 else 0)
    val extra = down * (pstrike + pspare) // extra points for previous frames

    // We play 10 frames, but have frames 11 (1 or 2 balls) and 12 (one frame only), and 13 (no more shots allowed)
    (frame, ball, down) match {
      case (12, 1, n) => Bowling(Some(points.get + n), 13, 0, 0, 0, 0)
      case (11, 1, n) if (n == 10 && pstrike > 0) => Bowling(Some(points.get + extra), 12, 1, 0, 0, 0)
      case (11, 1, n) if (pstrike > 0) => Bowling(Some(points.get + extra), 11, 2, n, 0, 0)
      case (11, _, n) => Bowling(Some(points.get + n), 13, 0, 0, 0, 0)
      case (10, 2, n) if (n + fshot < 10) => Bowling(Some(points.get + n), 13, 0, 0, 0, 0)
      case (f, 1, 10) => Bowling(Some(points.get + 10 + extra), f + 1, 1, 0, strikes, spares)
      case (f, 2, n) => Bowling(Some(points.get + n + extra), f + 1, 1, 0, strikes, spares)
      case (f, 1, n) => Bowling(Some(points.get + n + extra), f, 2, n, strikes, spares)
      case _ => Bowling(None)
    }
  }
}
