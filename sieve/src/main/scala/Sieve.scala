import java.util.stream.IntStream
object Sieve {
  lazy val p: Stream[Int] = 2 #:: Stream.from(3).filter(a => p.takeWhile(b => b * b <= a).forall(x => a % x != 0))
  def primes(n: Int): List[Int] = p.takeWhile(_ <= n).toList
}