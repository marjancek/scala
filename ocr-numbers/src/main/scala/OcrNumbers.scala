object OcrNumbers {

  // break in groups of 4 lines; join by comma
  def convert(scan: List[String]): String =
    scan.grouped(4).map(convertLine).mkString(",")

  def convertLine(scan: List[String]): String =
    if (scan.size == 0 || scan.size % 4 != 0
      || scan.head.size == 0 || scan.head.size % 3 != 0)
      return "?"
    else // turn sticks to codes, turn trios of codes to digits
      scan.transpose.map(column).mkString
        .grouped(3).map(translate).mkString

  //"     _   _       _   _   _   _   _   _ "
  //"  |  _|  _| |_| |_  |_    | |_| |_| | |"
  //"  | |_   _|   |  _| |_|   | |_|  _| |_|"
  //"                                       "
  //"001 234 031 451 432 132 061 131 431 171"

  // translates sticks to codes
  def column(c: List[Char]): String = c match {
    case ' ' :: ' ' :: ' ' :: ' ' :: xs => "0"
    case ' ' :: '|' :: '|' :: ' ' :: xs => "1"
    case ' ' :: ' ' :: '|' :: ' ' :: xs => "2"
    case '_' :: '_' :: '_' :: ' ' :: xs => "3"
    case ' ' :: '|' :: ' ' :: ' ' :: xs => "4"
    case ' ' :: '_' :: ' ' :: ' ' :: xs => "5"
    case '_' :: ' ' :: ' ' :: ' ' :: xs => "6"
    case '_' :: ' ' :: '_' :: ' ' :: xs => "7"
    case _ => "?"
  }

  // translates codes to numbers
  def translate(s: String): String = s match {
    case "001" => "1"
    case "234" => "2"
    case "031" => "3"
    case "451" => "4"
    case "432" => "5"
    case "132" => "6"
    case "061" => "7"
    case "131" => "8"
    case "431" => "9"
    case "171" => "0"
    case _ => "?"
  }

}