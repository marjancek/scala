object RotationalCipher {
  val WIDTH: Int = 'Z' - 'A' + 1

  def rotate(message: String, n: Int): String = {
    def shift(base: Char, c: Char): Char = base + ((c - base + n) % WIDTH) toChar

    def mapper(c: Char): Char =
      if (c.isUpper) shift('A', c)
      else if (c.isLower) shift('a', c)
      else c

    message.toList.map(mapper).mkString
  }
}