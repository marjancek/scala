object Series {
  def slices(n: Int, digits: String): List[List[Int]] = digits.map(_.asDigit).sliding(n).map(_.toList).toList
}