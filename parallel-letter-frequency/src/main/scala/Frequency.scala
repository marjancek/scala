import scala.collection.Map
import scala.concurrent.Future
import java.util.concurrent.Executors
import concurrent.ExecutionContext
import scala.concurrent.ExecutionContextExecutorService
import scala.concurrent.Await
import scala.concurrent.duration.Duration
import java.util.concurrent.TimeUnit

object Frequency {

  def frequency(numWorkers: Int, texts: Seq[String]): Map[Char, Int] = {
    val executorService = Executors.newFixedThreadPool(numWorkers)
    val executionContext = ExecutionContext.fromExecutorService(executorService)
    val futures = ("" +: texts).map(t => freq(t, executionContext)).toList // avoid reduceLeft error with empty entry

    Await.result(Future.reduceLeft(futures)((a, b) => a ++ (for ((k, v) <- b) yield (k -> (v + a.getOrElse(k, 0)))))
        (executionContext), Duration.create(10, TimeUnit.SECONDS))
  }

  def freq(text: String, executionContext: ExecutionContextExecutorService): Future[Map[Char, Int]] = Future(
    text.toLowerCase().replaceAll("[^a-züöëïä]", "").toSeq.par.groupBy(identity).mapValues(_.size).seq)(executionContext)
}
