object CollatzConjecture {

  def steps(num: Int, counter: Int = 0): Option[Int] =
    if (num <= 0) None
    else if (num == 1) Some(counter)
    else if (num % 2 == 0) steps(num / 2, counter + 1)
    else steps(3 * num + 1, counter + 1)

}