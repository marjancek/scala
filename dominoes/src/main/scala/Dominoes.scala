object Dominoes {
  type Stones = List[(Int, Int)]
  
  def chain(input: Stones): Option[Stones] = {
    if (input.length > 0) complete(input.head._2, List(input.head), input.tail, List())
    else Some(List())
  }
  
  // start of first stone; sorted stones so far, stones to choose from, stones that didn't fit for this sorted list
  def complete(start: Int, sorted: Stones, unsorted: Stones, discarded: Stones): Option[Stones] = {
    (sorted.head._1, unsorted) match {
      case (last, x :: xs) => {
        var found: Option[Stones] = None
        if (x._2 == last) found = complete(start, (x._1, x._2) :: sorted, discarded ++ xs, List())
        else if (x._1 == last) found = complete(start, (x._2, x._1) :: sorted, discarded ++ xs, List())

        if (found.isEmpty) complete(start, sorted, xs, x +: discarded)
        else found
      }
      case _ => if (discarded.length == 0 && start == sorted.head._1) Some(sorted) else None
    }
  }

}