object AllYourBase {

  def rebase(from: Int, digits: List[Int], to: Int): Option[List[Int]] =
    if (to < 2 || from < 2) None
    else try {
      Some(pack(unpack(digits, from), to))
    } catch {
      case e: Exception => None
    }

  def unpack(digits: List[Int], base: Int): Int = digits match {
    case x :: xs if (x < 0 || x >= base) => throw new Exception(s"invalid digit $x for base $base")
    case x :: xs => x * intPow(base, xs.length) + unpack(xs, base)
    case _ => 0
  }

  def pack(number: Int, base: Int): List[Int] = number match {
    case 0 => List(0)
    case n if (n < base) => List(n)
    case n => pack(n / base, base) :+ n % base
  }

  def intPow(base: Int, exp: Int): Int = exp match {
    case 0 => 1
    case p if (p % 2 == 0) => intPow(base * base, exp / 2)
    case n => base * intPow(base, exp - 1)
  }
}