object Minesweeper {

  def annotate(board: List[String]): List[String] =
    board.zipWithIndex.map({
      case (line, i) =>
        line.zipWithIndex.map({ case (char, j) => translate(char, i, j, board) }).mkString
    })

  def translate(char: Char, x: Int, y: Int, board: List[String]): Char =
    (char, countBombsAround(x, y, board)) match {
      case ('*', _) => '*'
      case (_, d) if (d > 0) => d.toString().charAt(0)
      case _ => ' '
    }

  def countBombsAround(x: Int, y: Int, board: List[String]): Int =
    (for (
      i <- x - 1 to x + 1;
      j <- y - 1 to y + 1;
      if (i >= 0 && i < board.size && j >= 0 && j < board.head.size)
    ) yield board(i)(j)).count(_ == '*')

}