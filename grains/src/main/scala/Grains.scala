object Grains {

  def square(n: Int): Option[BigInt] = n match {
    case 0 | -1 | 65 => None
    case 1 => Some(1)
    case x => Some(BigInt(2) * square(n-1).get)
  }

  def total(): BigInt = 1.to(64).map(square).map(_.get).sum 
  
}