object Diamond {

  val top: Char = 'A';

  def rows(mid: Char): List[String] = {
    val width: Int = 2 * (mid - top) + 1

    def addAll(c: Char, pic: List[String]): List[String] = {
      val row = level(c, width, (mid - c))
      c match {
        case 'A' => row +: pic :+ row
        case ch if (ch > 'A') => addAll((c - 1).toChar, row +: pic :+ row)
        case _ => pic
      }
    }
    
    addAll((mid - 1).toChar, List(level(mid, width, 0)))
  }

  def level(c: Char, width: Int, left: Int): String = {
    val centre = width - left - left - 2
    if (centre <= 0) " " * left + c + " " * left
    else " " * left + c + " " * centre + c + " " * left
  }

}