object RobotSimulator {
  def simulate(r: Robot, s: String): Robot = s.foldLeft(r)(action)

  def action(r: Robot, a: Char): Robot = a match {
    case 'A' => r.advance
    case 'L' => r.turnLeft
    case 'R' => r.turnRight
  }
}