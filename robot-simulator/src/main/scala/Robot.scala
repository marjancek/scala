case class Robot(bearing: Bearing.Bearing, coordinates: (Int, Int)) {
  //zoolander style
  def turnRight = Robot(leftOf(leftOf(leftOf(bearing))), coordinates)
  
  def turnLeft = Robot(leftOf(bearing), coordinates)
  
  def simulate(s: String) = RobotSimulator.simulate(this, s)
  
  def advance = Robot(bearing, posAhead)

  private def leftOf(dir: Bearing.Bearing): Bearing.Bearing = dir match {
    case Bearing.North => Bearing.West
    case Bearing.West => Bearing.South
    case Bearing.South => Bearing.East
    case Bearing.East => Bearing.North
  }

  private def posAhead: (Int, Int) = bearing match {
    case Bearing.North => (coordinates._1, coordinates._2 + 1)
    case Bearing.West => (coordinates._1 - 1, coordinates._2)
    case Bearing.South => (coordinates._1, coordinates._2 - 1)
    case Bearing.East => (coordinates._1 + 1, coordinates._2)
  }

  override def toString: String = coordinates.toString() + bearing.toString()

  override def equals(other: Any): Boolean = other match {
    case Robot(d, c) => d == bearing && c == coordinates
    case _ => false
  }

}

object Bearing extends Enumeration {
  type Bearing = Value
  val North, East, South, West = Value
}