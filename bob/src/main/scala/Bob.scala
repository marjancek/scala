object Bob {

  def allCaps(s: String): Boolean = s.matches(".*[a-zA-Z]+.*") && s == s.toUpperCase()

  def isQuestion(s: String): Boolean = s.trim().endsWith("?")

  def isSilence(s: String): Boolean = s.trim().isEmpty()

  def response(s: String): String = (isSilence(s), isQuestion(s), allCaps(s)) match{
    case (true, _,  _ )  => "Fine. Be that way!"
    case (false, true, true) => "Calm down, I know what I'm doing!"
    case (false, true, false) => "Sure."
    case (false, false, true) =>"Whoa, chill out!"
    case (false, false, false) => "Whatever."
  }

}
