object MatchingBrackets {

  /**
   * @param exp expression to analyse
   * @return true iif the parenthesis '()', '{}' and '[]' are matched correctly
   */
  def isPaired(exp: String): Boolean = {
    paring(exp.toList, Nil)
  }

  private def paring(left: List[Char], right: List[Char]): Boolean = {
    (left, right) match {
      // reached last character, we should have matched all symbols already
      case (Nil, Nil) => true
      // If we open a parenthesis, we put it on top of the ones to find to the right
      case ('{' :: xs, ys) => paring(xs, '}' :: ys)
      case ('(' :: xs, ys) => paring(xs, ')' :: ys)
      case ('[' :: xs, ys) => paring(xs, ']' :: ys)
      // If we close a parenthesis, make sure it was expected (on top), and continue with the rest
      case ('}' :: xs, y :: ys) => y.equals('}') && paring(xs, ys)
      case (')' :: xs, y :: ys) => y.equals(')') && paring(xs, ys)
      case (']' :: xs, y :: ys) => y.equals(']') && paring(xs, ys)
      // If it is something else, we don't care, and continue
      case (_ :: xs, ys) => paring(xs, ys)
      case _ => false
    }
  }

}