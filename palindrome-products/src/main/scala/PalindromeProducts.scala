class PalindromeProducts(start: Int, end: Int) {

  val allPalindromeProducts = for (
    a <- start.to(end);
    b <- a.to(end);
    val c = a * b;
    if (isPalindrome(c.toString()))
  ) yield (c, (a, b))

  val dict = allPalindromeProducts.groupBy(_._1).mapValues(_.map(_._2).toSet)

  def isPalindrome(number: String): Boolean = (number == number.reverse)

  def smallest: Option[(Int, Set[(Int, Int)])] = dict.find(_._1 == dict.keys.min)
  def largest: Option[(Int, Set[(Int, Int)])] = dict.find(_._1 == dict.keys.max)
}

object PalindromeProducts {
  def apply(from: Int, to: Int): PalindromeProducts = new PalindromeProducts(from, to)
}