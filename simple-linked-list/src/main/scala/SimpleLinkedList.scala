class SimpleLinkedList[T] {
  val element :Option[T] = Option.empty
  var nextOne: SimpleLinkedList[T] = null
  
  def isEmpty: Boolean = true
  def value: T = element.get
  def add(item: T): SimpleLinkedList[T] = new SomeSLL[T](item, this)
  def next: SimpleLinkedList[T] = nextOne
  def reverse: SimpleLinkedList[T] = this
  def toSeq: Seq[T] = Seq[T]()
}


case class SomeSLL[T](e: T, n:SimpleLinkedList[T] =null) extends SimpleLinkedList[T] {
  nextOne = n
  override def isEmpty: Boolean = false
  override def value: T = e
  override def add(item: T): SimpleLinkedList[T] = {nextOne = next.add(item); this }
  override def reverse : SimpleLinkedList[T] = SimpleLinkedList.fromSeq(this.toSeq.reverse)
  override def toSeq: Seq[T] =  e +: nextOne.toSeq
}

object SimpleLinkedList {
  def apply[T]() = new SimpleLinkedList[T]()
  def fromSeq[T](seq:Seq[T]): SimpleLinkedList[T] = seq.foldLeft(new SimpleLinkedList[T]())({case (sll, e) => sll.add(e)})
}
