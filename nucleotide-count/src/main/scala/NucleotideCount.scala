case class DNA(dna: String) {
  val valids = "ATCG"

  def nucleotideCounts: Either[Boolean, Map[Char, Int]] =
    if (!dna.forall(valids.contains(_)))
      Left(true)
    else
      Right(valids.map(_ -> 0).toMap ++ dna.groupBy(identity).mapValues(_.length()))
}