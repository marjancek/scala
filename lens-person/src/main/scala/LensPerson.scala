import java.time.LocalDate
import monocle.Lens
import monocle.macros.GenLens
import monocle.function.Cons.headOption

object LensPerson {
  case class Person(_name: Name, _born: Born, _address: Address)

  case class Name(_foreNames: String /*Space separated*/ , _surName: String)

  // Value of java.time.LocalDate.toEpochDay
  type EpochDay = Long

  case class Born(_bornAt: Address, _bornOn: EpochDay)

  case class Address(_street: String, _houseNumber: Int,
    _place: String /*Village / city*/ , _country: String)

  // Valid values of Gregorian are those for which 'java.time.LocalDate.of'
  // returns a valid LocalDate.
  case class Gregorian(_year: Int, _month: Int, _dayOfMonth: Int)

  // Implement these.
  val name: Lens[Person, Name] = GenLens[Person](_._name)
  val born: Lens[Person, Born] = GenLens[Person](_._born)
  val address: Lens[Person, Address] = GenLens[Person](_._address)

  val forenames: Lens[Name, String] = GenLens[Name](_._foreNames)
  val surname: Lens[Name, String] = GenLens[Name](_._surName)

  val bornAt: Lens[Born, Address] = GenLens[Born](_._bornAt)
  val bornOn: Lens[Born, EpochDay] = GenLens[Born](_._bornOn)

  val street: Lens[Address, String] = GenLens[Address](_._street)
  val number: Lens[Address, Int] = GenLens[Address](_._houseNumber)
  val place: Lens[Address, String] = GenLens[Address](_._place)
  val country: Lens[Address, String] = GenLens[Address](_._country)

  val bornStreet: Born => String =
    (birthPlace: Born) => bornAt.composeLens(street).get(birthPlace)

  val setCurrentStreet: String => Person => Person =
    (newStreet: String) => (p: Person) =>
      (address composeLens street).set(newStreet)(p)

  val setBirthMonth: Int => Person => Person =
    (newMonth: Int) => (p: Person) =>
      (born composeLens bornOn).modify(epochChangeMonthTo(_, newMonth))(p)

  def epochChangeMonthTo(epoch: EpochDay, month: Int): EpochDay = {
    val ld = LocalDate.ofEpochDay(epoch)
    LocalDate.of(ld.getYear(), month, ld.getDayOfMonth()).toEpochDay
  }

  // Transform both birth and current street names.
  val renameStreets: (String => String) => Person => Person =
    (f: String => String) => (p: Person) => (((address composeLens street).modify(f))
      compose ((born composeLens bornAt composeLens street).modify(f))).apply(p)
}
