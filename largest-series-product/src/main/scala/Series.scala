object Series {
  def largestProduct(n: Int, digits: String): Option[Int] = (n, digits.length()) match {
    case (0, _) => Some(1)
    case (x, y) if (x < 0 || x > y || !digits.replaceAll("[^0-9]", "").equals(digits)) => None
    case _ => Some(digits.map(_.asDigit).sliding(n).toList.map(_.product).max)
  }

}