object SecretHandshake {

  // generate binaries 1, 10, 100...; filter those that match the given code, send to translation
  def commands(n: Int): List[String] = translate(
    0.to(4).map(1 << _).filter(a => (n & a) > 0).toList,
    List())

  def translate(codes: List[Int], shakes: List[String]): List[String] = codes match {
    case 1 :: xs => translate(xs, shakes :+ "wink")
    case 2 :: xs => translate(xs, shakes :+ "double blink")
    case 4 :: xs => translate(xs, shakes :+ "close your eyes")
    case 8 :: xs => translate(xs, shakes :+ "jump")
    case 16 :: xs => shakes.reverse
    case _ => shakes
  }
}