object Acronym {
  def abbreviate(phrase: String): String = phrase.split("[\\s\\-]+").map(_.charAt(0).toUpper).mkString
}