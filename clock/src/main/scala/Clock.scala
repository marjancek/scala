class Clock(minutes: Int) {
  val mins = ((minutes % 1440) + 1440) % 1440 // make positive mod

  def +(other: Clock) = Clock(this.mins + other.mins)
  def -(other: Clock) = Clock(this.mins - other.mins)

  override def equals(other: Any) = other match {
    case o: Clock => this.mins == o.mins
    case _ => false
  }
}

object Clock {
  def apply(minutes: Int): Clock = new Clock(minutes)
  def apply(hours: Int, minutes: Int): Clock = new Clock(hours * 60 + minutes)
}