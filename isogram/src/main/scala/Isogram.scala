object Isogram {
  def isIsogram(s: String): Boolean = {
    val clean = s.toLowerCase().replaceAll("[^a-z]", "")
    clean.distinct.length() == clean.length()
  }
}