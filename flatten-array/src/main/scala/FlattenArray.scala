object FlattenArray {

  def flatten[_](s: List[_]): List[_] = s match {
    case Nil => Nil
    case (h: List[_]) :: xs => flatten(h) ::: flatten(xs)
    case null :: xs => flatten(xs)
    case h :: xs => h :: flatten(xs)
  }
  
}