import scala.util.matching.Regex

object RunLengthEncoding {

  def encode(input: String): String = {
    val repeat: Regex = "(.)\\1*"r

    repeat.findAllMatchIn(input).map(m => match2rep(m) + m.group(1)).mkString
  }

  def decode(s: String): String = {
    val pair: Regex = "([0-9]*)([^0-9])"r

    pair.findAllMatchIn(s).map(m => m.group(2) * rep2num(m.group(1))).mkString
  }

  def match2rep(m: Regex.Match): String =
    if (m.matched.length() <= 1) "" else m.matched.length().toString()

  def rep2num(n: String): Int = if (n.isEmpty()) 1 else n.toInt

}