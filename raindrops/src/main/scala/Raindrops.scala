object Raindrops {
  val translate = Map(3 -> "Pling", 5 -> "Plang", 7-> "Plong")
  
  def convert(n: Int): String = translate.keySet.filter(n%_==0).map(translate).mkString.replaceAll("^$", n.toString())
}

