object Luhn {
  def valid(n: String): Boolean = {
    lazy val validEntry = n.replaceAll("[0-9\\s]", "").length == 0
    lazy val cleaned = n.replace(" ", "").reverse
    lazy val sum = cleaned.chars()
      .map(_ - '0')
      .toArray()
      .zipWithIndex
      .map(transform)
      .sum

    validEntry && cleaned.length() > 1 && sum % 10 == 0;
  }

  def transform(e: (Int, Int)): Int =
    if (e._2 % 2 > 0) // odd index
      if (e._1 > 4) e._1 * 2 - 9 // double overflows
      else (e._1 * 2) // double doesn't overflow
    else e._1
}