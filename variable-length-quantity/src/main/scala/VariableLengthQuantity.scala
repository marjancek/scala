import java.util.Locale
import scala.annotation.tailrec

object VariableLengthQuantity {

  def encode(msg: List[Int]): List[Int] = {
    msg.flatMap(encodeNum(_))
  }

  @tailrec
  def encodeNum(n: Int, acc: List[Int] = List()): List[Int] =
    if (acc.isEmpty) encodeNum(n >>> 7, List(n & 0x7f))
    else if (n > 0) encodeNum(n >>> 7, ((n & 0x7f) | 0x80) +: acc)
    else acc

  @tailrec
  def decode(parts: List[Int], curr: Int = 0, acc: List[Int] = List()): Either[String, List[Int]] = parts match {
    case n :: nx => {
      val newCurr = (curr << 7) | (n & 0x7f)
      if ((n & 0x80) == 0) decode(nx, 0, acc :+ newCurr) // finished a number
      else decode(nx, newCurr, acc) // still collecting the current number
    }
    case _ if (acc.size > 0) => Right(acc)
    case _ => Left(curr.toString())
  }
}