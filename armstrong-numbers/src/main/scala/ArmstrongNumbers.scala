object ArmstrongNumbers {
  def isArmstrongNumber(num: Int): Boolean = num == calculateArmstrong(num.toString())
  def calculateArmstrong(digits: String): Int = digits.map(c => scala.math.pow(c - '0', digits.length).toInt).sum
}