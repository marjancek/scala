object AtbashCipher {

  def swap(c: Char): Char = if (c >= '0' && c <= '9') c
  else if (c >= 'a' && c <= 'z') ('z' - c + 'a').toChar
  else if (c >= 'A' && c <= 'Z') ('Z' - c + 'A').toChar
  else ' '

  def encode(s: String): String =
    s.replaceAll("[\\s\\.,]", "")
      .toLowerCase()
      .map(swap)
      .sliding(5, 5)
      .mkString(" ")

  def decode(s: String): String =
    s.replaceAll("[\\s\\.,]", "")
      .map(swap)
      .toString
}