object FoodChain {
  val iKnow = Vector(
    "I know an old lady who swallowed a fly.\n",
    "I know an old lady who swallowed a spider.\n",
    "I know an old lady who swallowed a bird.\n",
    "I know an old lady who swallowed a cat.\n",
    "I know an old lady who swallowed a dog.\n",
    "I know an old lady who swallowed a goat.\n",
    "I know an old lady who swallowed a cow.\n",
    "I know an old lady who swallowed a horse.\n")

  val did = Vector(
    "",
    "It wriggled and jiggled and tickled inside her.\n",
    "How absurd to swallow a bird!\n",
    "Imagine that, to swallow a cat!\n",
    "What a hog, to swallow a dog!\n",
    "Just opened her throat and swallowed a goat!\n",
    "I don't know how she swallowed a cow!\n",
    "She's dead, of course!\n")

  val swallow = Vector(
      "I don't know why she swallowed the fly. Perhaps she'll die.\n",
      "She swallowed the spider to catch the fly.\n",
      "She swallowed the bird to catch the spider that wriggled and jiggled and tickled inside her.\n",
      "She swallowed the cat to catch the bird.\n",
      "She swallowed the dog to catch the cat.\n",
      "She swallowed the goat to catch the dog.\n",
      "She swallowed the cow to catch the goat.\n")

      def recite(start: Int, end: Int) : String =  (start-1 to end-1).map(verse).mkString("\n") + "\n"
      
      def verse(n: Int) : String =  iKnow.apply(n)  + did.apply(n)  + (if(n<7)  swallow.take(n+1).reverse.mkString else "")

      
}