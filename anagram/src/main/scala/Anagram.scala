object Anagram {
  def findAnagrams(root: String, list: Seq[String]): Seq[String] =
    list.filter(b => anagram(b.toLowerCase(), root.toLowerCase()))

  def anagram(a: String, b: String): Boolean = !a.equals(b) && a.sorted.equals(b.sorted)
}