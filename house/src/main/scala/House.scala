object House {
  val pairs = List(
    ("", ""),
    ("house", "Jack built."),
    ("malt", "lay in"),
    ("rat", "ate"),
    ("cat", "killed"),
    ("dog", "worried"),
    ("cow with the crumpled horn", "tossed"),
    ("maiden all forlorn", "milked"),
    ("man all tattered and torn", "kissed"),
    ("priest all shaven and shorn", "married"),
    ("rooster that crowed in the morn", "woke"),
    ("farmer sowing his corn", "kept"),
    ("horse and the hound and the horn", "belonged to"))

  def recite(from: Int, end: Int): String = from to end map doVerse mkString "\n"

  def doVerse(from: Int): String = f"This is the ${pairs.apply(from)._1}" + from.until(1, -1).map(doPair).mkString + " that Jack built.\n"

  def doPair(num: Int): String = f" that ${pairs.apply(num)._2} the ${pairs.apply(num - 1)._1}"

}