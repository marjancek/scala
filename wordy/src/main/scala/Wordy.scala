object Wordy {

  val midX = "([\\-0-9]+) (plus|minus|divided by|multiplied by) ([\\-0-9]+)([^\\?]*\\?)"r
  val endX = "([\\-0-9]+)\\?"r
  val START = "What is "

  def answer(s: String): Option[Int] = {
    if (s.startsWith(START) && s.endsWith("?"))
      calc(s.drop(START.length()))
    else
      None
  }

  def calc(s: String): Option[Int] = s match {
    case midX(op1, x, op2, rest) =>  calc(operate(x, op1.toInt, op2.toInt)+rest)
    case endX(end) => Some(end.toInt)
    case _ => None
  }

  def operate(op: String, op1: Int, op2: Int): String =
    op match {
      case "plus" => (op1 + op2).toString()
      case "minus" => (op1 - op2).toString()
      case "multiplied by" => (op1 * op2).toString()
      case "divided by" => (op1 / op2).toString()
      case _ => "error"
    }

}