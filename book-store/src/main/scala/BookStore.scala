object BookStore {
  // converts list of books to list of quantities, then calculate with that
  def total(books: List[Int]): Int = calculate(books.groupBy(identity).map({ case (b, l) => l.length }).toList)

  // try to take the best next group possible (reduce), and then calculate price for the rest
  def calculate(counts: List[Int]): Int = reduce(counts) match {
    case (n, Nil) => price(n) // last n books to group
    case (n, remainder) => price(n) + calculate(remainder)
  }

  // give price for set of n books
  def price(n: Int): Int = 800 * n *
    (n match {
      case 0 => 0
      case 1 => 100
      case 2 => 95
      case 3 => 90
      case 4 => 80
      case 5 => 75
    }) / 100

  // Create the biggest possible group, and substract it from the whole set.
  // but give priority to groups of 4 over groups of 5 + reminders of 3 or more
  def reduce(counts: List[Int]): (Int, List[Int]) = {
    val sorted = counts.sorted
    val scrt = counts.filter(_ > sorted.head).length
    if (scrt >= 3 && scrt <= 4)
      (counts.length - 1, sorted.head +: sorted.tail.map(_ - 1).filter(_ > 0))
    else
      (counts.length, counts.map(_ - 1).filter(_ > 0))
  }
}