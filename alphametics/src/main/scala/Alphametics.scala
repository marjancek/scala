object Alphametics {

  val digits = List.range(0, 10).toSet

  def solve(formula: String): Option[Map[Char, Int]] = {
    val parts = formula.split("\\s==\\s")
    val result = parts.tail.head
    val terms = parts.head.split("\\s\\+\\s").toList
    val firstLeters = terms.map(_.head).+:(result.head)
    val alphabet = formula.replaceAll("[^A-Z]", "").sorted.distinct.toList

    def searchChars(letters: List[Char], mapping: Map[Char, Int]): Option[Map[Char, Int]] =
      letters match {
        case x :: xs => searchNums(xs, mapping, x).find(_.isDefined).getOrElse(None)
        case _ => if (tryMap(mapping)) Some(mapping) else None
      }

    def searchNums(letters: List[Char], mapping: Map[Char, Int], letter: Char): Set[Option[Map[Char, Int]]] =
      for (y <- (digits.--(mapping.values)) if ((!(y == 0)) || !firstLeters.contains(letter)))
        yield searchChars(letters, mapping + (letter -> y))

    def tryMap(m: Map[Char, Int]): Boolean = terms.map(w => translateWord(w, m)).sum == translateWord(result, m)

    def translateWord(word: String, m: Map[Char, Int]): Long =
      word.reverse.map(m).zipWithIndex.map({ case (c, p) => c * Math.pow(10, p) }).sum.toLong

    searchChars(alphabet, Map())
  }
}