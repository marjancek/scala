import scala.util.Random

case class Cipher(key: String) {
  private val LIMIT = (1 + 'z' - 'a') // zero-based limit for characters

  def encode(msg: String): String = process(shiftRight)(msg)
  def decode(msg: String): String = process(shiftLeft)(msg)
  // with shift function, process key/msg pairs
  def process(shift: ((Char, Char)) => Char)(s: String): String =  (key zip s).map(shift).mkString
  // shift right and left; handling wrap-around with Mod operand
  def shiftRight(p: (Char, Char)): Char = (((p._1 -'a'+ p._2-'a') % LIMIT) + 'a').toChar
  def shiftLeft(p: (Char, Char)): Char = (((p._2 - p._1 + LIMIT) % LIMIT) + 'a').toChar
}

object Cipher {
  def newKey: String = Random.alphanumeric.filter(_.isLower).take(10).mkString

  def apply(keyOp: Option[String]): Cipher = if (keyOp.isEmpty) Cipher(newKey)
  else if (keyOp.get.isEmpty() || keyOp.get != keyOp.get.replaceAll("[^a-z]", ""))
    throw new IllegalArgumentException("Bad or missing key parameter; lowercase string required")
  else Cipher(keyOp.get)
}