import Color.Color
import scala.annotation.tailrec

  case class Coord(x: Int, y: Int)
class Connect(board: Map[Coord, Char]) {
  
  def winner: Option[Color] = {
    val maxX = board.map(_._1.x).max
    val maxY = board.map(_._1.y).max
    val whiteStart = board.filter(p => p._1.x == 0 && p._2 == 'O').map(_._1).toList
    val backStart = board.filter(p => p._1.y == 0 && p._2 == 'X').map(_._1).toList

    if (reachables('O', whiteStart).exists(elem => elem.x == maxX))
      Some(Color.White)
    else if (reachables('X', backStart).exists(elem => elem.y == maxY))
      Some(Color.Black)
    else
      None
  }

  @tailrec
  final def reachables(c: Char, look: List[Coord], visited: Set[Coord] = Set()): Set[Coord] = look match {
    case x :: xs => reachables(c, xs ++ getFriends(c, x).filter(c => !(visited.contains(c))), visited + x)
    case _ => visited
  }

  def getFriends(c: Char, p: Coord): List[Coord] = 
    Connect.directions.map(o => Coord(p.x + o.x, p.y + o.y)).filter(d => board.getOrElse(d, '.') == c)
}

object Connect {
  final val directions = List(Coord(1, 0), Coord(-1, 0), Coord(0, 1), Coord(0, -1), Coord(-1, 1), Coord(1, -1))
  // convert list of string to Map (x,y)->Char
  def apply(board: List[String]): Connect = new Connect(board.map(_.toList).zipWithIndex
    .map(l => l._1.zipWithIndex.map(c => (Coord(l._2, c._2), c._1)).toMap)
    .foldLeft(Map[Coord, Char]())({ case (m, v) => m ++ v }))
}

object Color extends Enumeration {
  type Color = Value
  val White, Black, Gray = Value
}
