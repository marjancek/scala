case class Queen(x: Int, y: Int)

object Queen {
  def isValidCoord(z: Int): Boolean = 0 <= z && z < 8

  def create(x: Int, y: Int): Option[Queen] =
    if (!isValidCoord(x) || !isValidCoord(y)) None
    else Some(new Queen(x, y))
}

object QueenAttack {
  def canAttack(q1: Queen, q2: Queen): Boolean =
    q1.x == q2.x || q1.y == q2.y || q1.x - q1.y == q2.x - q2.y || q1.x + q1.y == q2.x + q2.y
}