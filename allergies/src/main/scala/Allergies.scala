object Allergies {
  def allergicTo(a: Allergen.Allergen, code: Int): Boolean =
    (a.id.toInt & code) > 0
    
  def list(code: Int): List[Allergen.Allergen] =
    Allergen.values.filter(allergicTo(_, code)).toList
}
object Allergen extends Enumeration {
  type Allergen = Value
  
  val Eggs = Value(1)
  val Peanuts = Value(2)
  val Shellfish = Value(4)
  val Strawberries = Value(8)
  val Tomatoes = Value(16)
  val Chocolate = Value(32)
  val Pollen = Value(64)
  val Cats = Value(128)
}