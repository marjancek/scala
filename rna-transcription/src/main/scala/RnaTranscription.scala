object RnaTranscription {
  val translator = Map(('A' -> 'U'), ('C' -> 'G'), ('G' -> 'C'), ('T' -> 'A'))
  def toRna(dna: String): Option[String] = Some(dna map translator)
}