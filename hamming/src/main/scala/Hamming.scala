object Hamming {
  def distance(x: String, y: String): Option[Int] =
    if (x.length() != y.length()) None
    else Some((x zip y).count({ case (a, b) => a != b }))
}