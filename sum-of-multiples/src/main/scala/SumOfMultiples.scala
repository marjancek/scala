object SumOfMultiples {
  // Iterative solution
  def sum(factors: Set[Int], limit: Int): Int =  (1 to limit-1).filter(n => factors.exists( n % _ == 0)).sum 
                                                                    //tailSum(factors, limit-1, 0)
  // Solution with Tail recursion
  def tailSum(factors: Set[Int], limit: Int, accum:Int): Int =
        if (limit<1)  accum
        else  tailSum(factors, limit-1, accum + (if (factors.exists(limit%_==0) ) limit else 0))    
 }

