case class ComplexNumber(real: Double, imaginary: Double) {
  def +(other: ComplexNumber): ComplexNumber = ComplexNumber(real + other.real, imaginary + other.imaginary)
  def -(other: ComplexNumber): ComplexNumber = ComplexNumber(real - other.real, imaginary - other.imaginary)
  def *(other: ComplexNumber): ComplexNumber = ComplexNumber(
    (real * other.real - imaginary * other.imaginary),
    (real * other.imaginary + imaginary * other.real))
  def /(other: ComplexNumber): ComplexNumber = this * (other.conjugate / other.square)
  def /(divisor: Double): ComplexNumber = ComplexNumber(real / divisor, imaginary / divisor)
  def conjugate: ComplexNumber = ComplexNumber(real, -imaginary)
  def square: Double = real * real + imaginary * imaginary
  def abs: Double = Math.sqrt(square)
}

object ComplexNumber {
  def apply(real: Double = 0.0, imaginary: Double = 0.0): ComplexNumber = new ComplexNumber(real, imaginary)
  def apply(real: Double): ComplexNumber = new ComplexNumber(real, 0.0)
  def exp(base: ComplexNumber): ComplexNumber = {
    val er = Math.exp(base.real)
    ComplexNumber(er * Math.cos(base.imaginary), er * Math.sin(base.imaginary))
  }
}