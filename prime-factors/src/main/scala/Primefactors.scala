object PrimeFactors {
  
  def factors(Num: Long, divisors : List[Long]= List(), look: Long=2) : List[Long] = (Num, look) match {
    case (n, l) if (n<l) => divisors
    case (_, l) if (Num%l ==0) => factors(Num/l, divisors :+ l, look)
    case _ => factors(Num, divisors, look+1)
  }
  
}