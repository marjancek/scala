
object PerfectNumbers {

  // Num with capital N to be able to use it in match
  def classify(Num: Int): Either[String, NumberType.Type] =
    if (Num <= 0) Left("Classification is only possible for natural numbers.")
    else sumOfDivisors(Num) match {
      case Num => Right(NumberType.Perfect)
      case s @ _ if s > Num => Right(NumberType.Abundant)
      case _ => Right(NumberType.Deficient)
    }

  def sumOfDivisors(num: Int): Int = (1 to num-1).filter(num % _ == 0).sum
}

object NumberType {
  sealed trait Type
  case object Perfect extends Type
  case object Abundant extends Type
  case object Deficient extends Type
}