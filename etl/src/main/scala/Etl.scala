object Etl {
  def transform(legacy: Map[Int, Seq[String]]): Map[String, Int] =
    for {
      (points, seq) <- legacy
      str <- seq
    } yield (str.toLowerCase(), points)
}