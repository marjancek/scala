object PascalsTriangle {

  def rows(n: Int): List[List[Int]] = List.iterate(List(1), n) {
    l => 1 +: (l zip l.tail).map(Function.tupled(_+_)) :+ 1
  }

}