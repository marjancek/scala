case class Garden(pots: List[List[Char]]) {
  def plants(name: String): List[Plant.Value] = pots.drop(order(name)).head.map(Plant.fromValue(_))
  def order(name: String): Int = name.charAt(0) - 'A'
}

object Garden {
  def defaultGarden(cups: String): Garden =
    Garden(cups.split("\n").toList.map(_.sliding(2, 2).toList).transpose.map(_.flatten))
}

object Plant extends Enumeration {
  type Main = Value

  val Clover = Value('C')
  val Grass = Value('G')
  val Radishes = Value('R')
  val Violets = Value('V')

  def fromValue(c: Char): Plant.Value = values.find(_.id == c).get
}