case class WordCount(s: String) {
  val brks = "\\'*[\\s,:;!?&@\\$%\\^\\.]+\\'*"

  def countWords(): Map[String, Int] = s.split(brks).filter(_.nonEmpty).toSeq.groupBy(_.toLowerCase).mapValues(_.size)
}