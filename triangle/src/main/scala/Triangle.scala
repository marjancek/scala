case class Triangle(sides: Double*) {
  def equilateral: Boolean = valid && sides.distinct.length == 1

  def isosceles: Boolean = valid && sides.distinct.length <= 2

  def scalene: Boolean = valid && sides.distinct.length == 3

  def valid: Boolean = sides.forall(_ > 0.0) && sides.sum / 2 > sides.max
}