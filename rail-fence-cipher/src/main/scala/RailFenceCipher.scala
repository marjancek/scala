object RailFenceCipher {

  // pick up the letters in the order calculated for enconding
  def encode(msg: String, lines: Int): String =
    groupedIndexes(msg.size, lines).map(msg(_)).mkString

  def decode(msg: String, lines: Int): String = {
    // get encoding sequence
    val ndx = groupedIndexes(msg.size, lines)
    // swap encoding indexes with destination indexes
    val xdn = ndx.zipWithIndex.sortBy(_._1).map(_._2)
    // decode
    xdn.map(msg(_)).mkString
  }

  // group letter indexes by their line number; keep only the indexes
  def groupedIndexes(size: Int, lines: Int): List[Int] =
    zipNums(Seq.range(0, size), lines - 1, 0, 1).sortBy(_._2).map(_._1)

  // gives letter indexes their line number, going from i towards **dir**,  change direction at ends
  def zipNums(msg: Seq[Int], n: Int, i: Int, dir: Int): List[(Int, Int)] =
    (msg, i, dir) match {
      case (x :: xs, 0, _) => (x, 0) +: zipNums(xs, n, 1, 1) // dir=1
      case (x :: xs, `n`, _) => (x, n) +: zipNums(xs, n, n - 1, -1) // dir=-1
      case (x :: xs, i, dir) => (x, i) +: zipNums(xs, n, i + dir, dir) // keep direction
      case _ => List()
    }

}