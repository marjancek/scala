class CustomSet(list: List[Int]) {

  private def getList(): List[Int] = list
}

object CustomSet {

  def fromList(list: List[Int]): CustomSet =
    new CustomSet(list.distinct.sorted);

  def empty(set: CustomSet): Boolean =
    set.getList().isEmpty

  def member(set: CustomSet, e: Int): Boolean =
    set.getList().contains(e)

  def isSubsetOf(sub: CustomSet, set: CustomSet): Boolean =
    set.getList().containsSlice(sub.getList())

  def isDisjointFrom(set1: CustomSet, set2: CustomSet): Boolean =
    intersection(set1, set2).getList().isEmpty

  def isEqual(set1: CustomSet, set2: CustomSet): Boolean =
    isSubsetOf(set1, set2) && isSubsetOf(set2, set1)

  def insert(set: CustomSet, e: Int): CustomSet =
    fromList(e +: set.getList())

  def intersection(set1: CustomSet, set2: CustomSet): CustomSet =
    fromList(set1.getList().filter(e => set2.getList().contains(e)))

  def difference(org: CustomSet, rest: CustomSet): CustomSet =
    fromList(org.getList().filterNot(e => rest.getList().contains(e)))

  def union(set1: CustomSet, set2: CustomSet): CustomSet =
    fromList(set1.getList() ++ set2.getList())
}