import scala.util.parsing.combinator.RegexParsers
import scala.annotation.tailrec

// A better solution could be created with scala.utils.parsing
// https://github.com/scala/scala-parser-combinators
// implementation does not comply with some non-logical unit tests
object Sgf extends RegexParsers {

  type Tree[A] = Node[A] // to separate the type from the constructor, cf. Haskell's Data.Tree
  type Forest[A] = List[Tree[A]]
  case class Node[A](rootLabel: A, subForest: Forest[A] = List())

  // A tree of nodes.
  type SgfTree = Tree[SgfNode]

  // A node is a property list, each key can only occur once.
  // Keys may have multiple values associated with them.
  type SgfNode = Map[String, List[String]]

  // parsing state machine: depth, current Sgf Node, stack of generated trees so far.
  // will walk the string, adding to the stack the newly found head (root with property names and properties)
  // going deeper when "(;" or ";" is found, and reducing the stack when ")" or $ found
  case class State(deep: Int, root: SgfNode, subs: List[SgfTree] = List())

  val startR = "(\\(;|;)([^;\\(\\)]*)(.*)"r // start of a node (";" or "(;"), the Node, and child/others
  val endR = "\\)(.*)|$"r // the end of a node (must reduce stack; pick all gathered children)
  @tailrec
  def parseSgf(text: String, n: Int = 0, sm: List[State] = List()): Option[SgfTree] = text match {
    case startR(pre, node, rest) => parseNode(node) match {
      case Some(state) => parseSgf(rest, n + 1, new State(n, state) +: sm)
      case _ => None
    }
    case endR(rest) if (n > 0) => parseSgf(rest, n - 1, reduce(n, sm)) // found an end of node-> reduce
    case _ if (sm.size == 1) => Some(Node(sm.head.root, sm.head.subs)) // reduced up to root -> end
    case _ => None // something went wrong, bad formed string
  }

  def reduce(n: Int, stack: List[State]): List[State] = {
    val subs = stack.takeWhile(_.deep == n).map(s => new Node(s.root, s.subs)) // pick all gathered children
    val oldHead = stack.drop(subs.size).head // and add them to their root on the stack
    val newHead = new State(oldHead.deep, oldHead.root, oldHead.subs ++ subs.reverse)
    newHead +: stack.drop(subs.size + 1) // update stack without old children, new head, and the rest
  }

  val nameR = "([A-Z]+)(\\[.*)"r // property name in the node
  val propsR = "\\[([\\[|\\]|\\\\\\sa-zA-Z]+?)\\](.*)"r // property values
  @tailrec
  def parseNode(text: String, curr: String = "", props: List[String] = List(), acc: SgfNode = Map()): Option[SgfNode] = text match {
    case nameR(name, rest) => { // found new property name: wrap up previous proprs if they exist
      val newAcc = if (curr.length > 0) acc + (curr -> props) else acc
      parseNode(rest, name, List(), newAcc)
    }
    case propsR(prop, rest) => parseNode(rest, curr, props :+ prop, acc) // found another property, add it to accumulator
    case "" => if (curr.length > 0) Some(acc + (curr -> props)) else Some(acc) //consumed everything -> end
    case _ => None
  }

  def main(args: Array[String]): Unit = {
    println(parseSgf("(;XX[xx][xxx](;YY[yy];ZZ[zz])(;AA[aa](;BB[bb])(;CC[cc])))"))
    println(parseSgf("(;A[B](;B[C])(;C[D]))"))
    println(parseSgf("(;)"))
  }
}