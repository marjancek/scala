object SpaceAge {
  def onMercury(day: Double): Double = onEarth(day) / 0.2408467
  def onVenus(day: Double): Double = onEarth(day) / 0.61519726
  def onEarth(day: Double): Double = day / 60 / 60 / 24 / 365.25
  def onMars(day: Double): Double = onEarth(day) / 1.8808158
  def onJupiter(day: Double): Double = onEarth(day) / 11.862615
  def onSaturn(day: Double): Double = onEarth(day) / 29.447498
  def onUranus(day: Double): Double = onEarth(day) / 84.016846
  def onNeptune(day: Double): Double = onEarth(day) / 164.79132
}