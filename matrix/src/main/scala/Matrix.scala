case class Matrix(txt: String) {
  // split string into lines; map each line into a Vector
  val vals = txt.split("\n").map(line => line.split(" ").map(_.toInt).toVector)
  def row(r: Int): Vector[Int] = vals.apply(r)
  def column(r: Int): Vector[Int] = vals.map(v => v.apply(r)).toVector
}