object BinarySearch {
  def find(data: List[Int], e: Int, offset: Int = 0): Option[Int] = data.size match {
    case 0 => None
    case 1 => if (data.head == e) Some(offset) else None
    case n => {
      val m = n / 2
      val v = data.apply(m)
      if (v == e) Some(m + offset)
      else if (v < e) find(data.drop(m), e, m + offset)
      else find(data.take(m), e)
    }
  }

}