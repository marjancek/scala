object SpiralMatrix {
  def spiralMatrix(n: Int): List[List[Int]] = {
    val m = Array.ofDim[Int](n, n)
    val squares = Math.ceil(n / 2.0).toInt // number of concentric squares

    def widthAt(p: Int): Int = (n - 1 - 2 * p)
    def startAt(layer: Int): Int = (0 until layer).map(p => 4 * widthAt(p)).sum + 1

    // paint this border, starting in m(p,p)
    def paint(p: Int, c: Int): Unit = {
      (0 to widthAt(p)).foreach(y => m(p)(p + y) = c + y); // going right
      (1 to widthAt(p)).foreach(y => m(p + y)(n - 1 - p) = c + widthAt(p) + y) // going down
      (1 to widthAt(p)).foreach(y => m(n - 1 - p)(n - 1 - p - y) = c + 2 * widthAt(p) + y) // going left
      (1 until widthAt(p)).foreach(y => m(n - 1 - p - y)(p) = c + 3 * widthAt(p) + y) // going up
    }
    // 'paint' with numbers the border of each concentric square, from the outside to the inside
    (0 until squares).foreach(p => paint(p, startAt(p)))
    m.toList.map(_.toList).toList
  }
}