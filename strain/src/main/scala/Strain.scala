// Keep your hands off that filter/reject/whatchamacallit functionality provided by your standard library!
// Solve this one yourself using other basic tools instead.
object Strain {
  def keep[T](s: Seq[T], f: (T) => Boolean): Seq[T] = { for (e <- s if f(e)) yield e }
  def discard[T](s: Seq[T], f: (T) => Boolean): Seq[T] = keep(s, !f(_: T))
}