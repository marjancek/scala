object ProteinTranslation {

  def proteins(s: String): Seq[String] = s.grouped(3).map(decode).takeWhile(_ != "STOP").toSeq

  def decode(s: String): String = s match {
    case "AUG" => "Methionine"
    case "UUU" | "UUC" => "Phenylalanine"
    case "UUA" | "UUG" => "Leucine"
    case "UCU" | "UCC" | "UCA" | "UCG" => "Serine"
    case "UAU" | "UAC" => "Tyrosine"
    case "UGU" | "UGC" => "Cysteine"
    case "UGG" => "Tryptophan"
    case "UAA" | "UAG" | "UGA" => "STOP"
  }

}