object PythagoreanTriplet {

  type Trio = (Int, Int, Int)

  def isPythagorean(tri: Trio): Boolean = {
    val cuads = tri.productIterator.toList.map({ case x: Int => x * x })
    cuads.sum == 2 * cuads.max
  }

  def pythagoreanTriplets(lower: Int, upper: Int): Seq[Trio] =
    for (
      a <- (lower to upper - 2);
      b <- (a + 1 to upper - 1);
      c <- (b + 1 to upper);
      if (a * a + b * b == c * c)
    ) yield (a, b, c)

}