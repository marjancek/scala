import scala.annotation.tailrec

object Zipper {
  // A zipper for a binary tree.

  // We need to keep a stack of all ancestors, but also whether we got there by a left or right turn
  // The ancestors will be used to move up, reconstructing them because be might have changed them
  // and move up to the root to get the full tree again.
  sealed trait Ancestor[A] { val value: A; val tree: Option[BinTree[A]] }
  case class Lefty[A](value: A, tree: Option[BinTree[A]]) extends Ancestor[A]
  case class Righty[A](value: A, tree: Option[BinTree[A]]) extends Ancestor[A]

  // A zipper is the current binary tree, plus the stack of ancestors
  case class Zipper[A](curr: BinTree[A], ancestors: List[Ancestor[A]] = List())

  // Get a zipper focussed on the root node.
  def fromTree[A](bt: BinTree[A]): Zipper[A] = new Zipper(bt)

  // Get the complete tree from a zipper.
  @tailrec
  def toTree[A](zipper: Zipper[A]): BinTree[A] =
    if (zipper.ancestors.size > 0) toTree(up(zipper).get)
    else zipper.curr

  // Get the value of the focus node.
  def value[A](zipper: Zipper[A]): A = zipper.curr.value

  // Get the left child of the focus node, if any.
  def left[A](zipper: Zipper[A]): Option[Zipper[A]] = zipper match {
    case Zipper(BinTree(v, Some(l), r), xs) => Some(Zipper[A](l, Lefty(v, r) +: xs))
    case _ => None
  }

  // Get the right child of the focus node, if any.
  def right[A](zipper: Zipper[A]): Option[Zipper[A]] = zipper match {
    case Zipper(BinTree(v, l, Some(r)), xs) => Some(Zipper[A](r, Righty(v, l) +: xs))
    case _ => None
  }

  // Get the parent of the focus node, if any.
  def up[A](zipper: Zipper[A]): Option[Zipper[A]] = zipper match {
    case Zipper(c, Righty(v, l) :: xs) => Some(Zipper[A](BinTree(v, l, Some(c)), xs))
    case Zipper(c, Lefty(v, r) :: xs) => Some(Zipper[A](BinTree(v, Some(c), r), xs))
    case _ => None
  }

  // Set the value of the focus node.
  def setValue[A](v: A, zipper: Zipper[A]): Zipper[A] = zipper match {
    case Zipper(BinTree(_, l, r), xs) => Zipper(BinTree(v, l, r), xs)
  }

  // Replace a left child tree.
  def setLeft[A](l: Option[BinTree[A]], zipper: Zipper[A]): Zipper[A] = zipper match {
    case Zipper(BinTree(v, _, r), xs) => Zipper(BinTree(v, l, r), xs)
  }

  // Replace a right child tree.
  def setRight[A](r: Option[BinTree[A]], zipper: Zipper[A]): Zipper[A] = zipper match {
    case Zipper(BinTree(v, l, _), xs) => Zipper(BinTree(v, l, r), xs)
  }
}

// A binary tree.
case class BinTree[A](value: A, left: Option[BinTree[A]], right: Option[BinTree[A]])

