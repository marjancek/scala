object PhoneNumber {

  def clean(s: String): Option[String] = {
    val pattern = "^\\+?1?\\s*\\(?([2-9]\\d\\d)\\)?\\s*-?\\.?([2-9]\\d\\d)\\s*-?\\.?(\\d\\d\\d\\d)".r
    s.trim() match {
      case pattern(area, exchange, subscriber) => Some(area + exchange + subscriber)
      case _ => None
    }
  }

}