object Pangrams {
  // Checks all letters from a to z are at least once in the input string in lower case
  def isPangram(input: String): Boolean = 'a'.to('z').forall(p => input.toLowerCase().indexOf(p) >= 0)
}

