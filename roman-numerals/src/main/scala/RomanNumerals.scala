object RomanNumerals {

  def unfoldRight[A, B](start: B)(fun: B => Option[(A, B)]): List[A] = fun(start) match {
    case Some((drop, chain)) => drop +: unfoldRight(chain)(fun)
    case None => List()
  }

  def romanHigher(n: Int): Option[(Char, Int)] =
    if (n >= 1000) Some('M', n - 1000)
    else if (n >= 900) Some('C', n + 100)
    else if (n >= 500) Some('D', n - 500)
    else if (n >= 400) Some('C', n + 100)
    else if (n >= 100) Some('C', n - 100)
    else if (n >= 90) Some('X', n + 10)
    else if (n >= 50) Some('L', n - 50)
    else if (n >= 40) Some('X', n + 10)
    else if (n >= 10) Some('X', n - 10)
    else if (n >= 9) Some('I', n + 1)
    else if (n >= 5) Some('V', n - 5)
    else if (n >= 4) Some('I', n + 1)
    else if (n >= 1) Some('I', n - 1)
    else None

  def roman(num: Int): String = unfoldRight(num)(romanHigher) mkString
}
