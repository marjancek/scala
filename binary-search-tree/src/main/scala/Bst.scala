trait Bst[T] {
  def insert(v: T): Bst[T]
  def value: T
  def left: Option[Bst[T]]
  def right: Option[Bst[T]]
}

case class BstEmpty[T: Ordering]() extends Bst[T] {
  def insert(v: T): Bst[T] = new BstWith[T](v, Some(new BstEmpty[T]()), Some(new BstEmpty[T]()))
  def left: Option[Bst[T]] = None
  def right: Option[Bst[T]] = None
  def value: T = throw new Exception("")
}

case class BstWith[T: Ordering](value: T, left: Option[Bst[T]], right: Option[Bst[T]]) extends Bst[T] {
  val order = implicitly[Ordering[T]] // needed to compare things around
  def insert(v: T): Bst[T] = {
    if (order.gt(v, value)) new BstWith(value, left, Some(right.get.insert(v)))
    else new BstWith(value, Some(left.get.insert(v)), right)
  }
}

object Bst {
  def apply[T: Ordering](value: T): Bst[T] = new BstWith(value, Some(new BstEmpty[T]), Some(new BstEmpty[T]))
  def fromList[T: Ordering](list: List[T]): Bst[T] = list.tail.foldLeft(Bst(list.head))((t, v) => t.insert(v))
  def toList[T](tree: Bst[T]): List[T] = tree match {
    case BstEmpty() => List()
    case t @ BstWith(v, l, r) => toList(l.get) ::: v :: toList(r.get)
  }
}

