import java.time.{ DayOfWeek, LocalDate }

import Schedule.Schedule

case class Meetup(month: Int, year: Int) {
  val start = LocalDate.of(year, month, 1)
  /** Calculates the first searched day by offset from given day of week (+7,%7 to get a positive result) */
  def findFirst(dow: Int, from: LocalDate) = from.plusDays((dow-from.getDayOfWeek.getValue+ 7) % 7)

  def day(dayOfWeek: Int, schedule: Schedule): LocalDate = schedule match {
    case Schedule.First => findFirst(dayOfWeek, start)
    case Schedule.Second => findFirst(dayOfWeek, start.plusDays(7))
    case Schedule.Third => findFirst(dayOfWeek, start.plusDays(14))
    case Schedule.Fourth => findFirst(dayOfWeek, start.plusDays(21))
    case Schedule.Last => findFirst(dayOfWeek, start.plusMonths(1).plusDays(-7))
    case Schedule.Teenth => findFirst(dayOfWeek, start.plusDays(12))
  }
}

object Schedule extends Enumeration {
  type Schedule = Value
  val Teenth, First, Second, Third, Fourth, Last = Value
}

object Meetup {
  val Mon = DayOfWeek.MONDAY.getValue
  val Tue = DayOfWeek.TUESDAY.getValue
  val Wed = DayOfWeek.WEDNESDAY.getValue
  val Thu = DayOfWeek.THURSDAY.getValue
  val Fri = DayOfWeek.FRIDAY.getValue
  val Sat = DayOfWeek.SATURDAY.getValue
  val Sun = DayOfWeek.SUNDAY.getValue
}
