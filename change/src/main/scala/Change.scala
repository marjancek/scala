import scala.annotation.tailrec

object Change {

  type Entry = (Int, List[Int])
  type Partials = Map[Int, List[Int]]

  // Greedy algorithm on coins, with Stack of partial solutions:
  // 0- sort coins descending
  // 1- start with empty partial solution with original value to resolve
  // 2- update all existing partial solutions with highest coin while possible
  // 3- put old partial solutions in map, keeping the shortest path to that value
  //  4- when all old and new partial solutions have been evaluated with this coin ->
  //   remove coin and start with next biggest coin and existing partial solutions sorted by value
  def findFewestCoins(value: Int, coins: List[Int]): Option[List[Int]] =
    breathFirst(coins.sortWith(_ > _), Map.empty, List((value, List.empty)))
    
  @tailrec
  def breathFirst(coins: List[Int], done: Partials, stack: List[Entry]): Option[List[Int]] =
    (coins, stack) match {
      case (_, (0, solution) :: ns) => Some(solution)
      case (c :: cs, n :: ns) if (c <= n._1) => breathFirst(coins, addShortest(done, n), ns :+ (n._1 - c, c +: n._2))
      case (c :: cs, n :: ns) => breathFirst(coins, addShortest(done, n), ns)
      case (c :: cs, _) => breathFirst(cs, Map.empty, done.toList.sortBy(_._1))
      case _ => None
    }

  // there's probably a better way to do this...
  def addShortest(map: Partials, node: Entry): Partials =
    if (map.getOrElse(node._1, node._2).size >= node._2.size)
      map + node
    else
      map
}