class School {
  type DB = Map[Int, Seq[String]]
  var database: DB = Map();

  def db: DB = database

  def grade(g: Int): Seq[String] = db.getOrElse(g, Seq.empty)

  def add(name: String, g: Int) = database += (g -> (grade(g) :+ name))

  def sorted: DB = db.toSeq.map(e => e._1 -> e._2.sorted).sortBy(_._1).toMap
}

