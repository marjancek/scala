object CryptoSquare {
  def ciphertext(s: String): String = {
    val clean = s.toLowerCase().replaceAll("[^a-z0-9]", "")
    val width = Math.ceil(Math.sqrt(clean.length())).toInt
    if (width > 0) {
      val missing = (width * width - clean.length()) % width
      val rectangle = clean + " " * missing
      rectangle.sliding(width, width).toList.transpose.map(m => m.mkString).mkString(" ")
    } else ""
  }
}