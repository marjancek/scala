object Sublist {
  trait ListComparison
  case object Equal extends ListComparison
  case object Sublist extends ListComparison
  case object Unequal extends ListComparison
  case object Superlist extends ListComparison

  def sublist(a: List[Int], b: List[Int]): ListComparison =
    if (a == b) Equal
    else if (a.containsSlice(b)) Superlist
    else if (b.containsSlice(a)) Sublist
    else Unequal

}