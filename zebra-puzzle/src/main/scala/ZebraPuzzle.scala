object ZebraPuzzle {

  sealed trait Resident
  case object Englishman extends Resident
  case object Spaniard extends Resident
  case object Ukrainian extends Resident
  case object Norwegian extends Resident
  case object Japanese extends Resident

  sealed trait Drink
  case object Water extends Drink
  case object Milk extends Drink
  case object Coffee extends Drink
  case object Tea extends Drink
  case object Orange extends Drink

  sealed trait Colour
  case object Red extends Colour
  case object Green extends Colour
  case object Blue extends Colour
  case object Yellow extends Colour
  case object Ivory extends Colour

  sealed trait Smoke
  case object OldGold extends Smoke
  case object Kools extends Smoke
  case object Lucky extends Smoke
  case object Chesterfields extends Smoke
  case object Parliaments extends Smoke

  sealed trait Pet
  case object Dog extends Pet
  case object Fox extends Pet
  case object Horse extends Pet
  case object Snail extends Pet
  case object Zebra extends Pet

  // House numbers 0 to 4, index in each sequence

  case class Solution(waterDrinker: Resident, zebraOwner: Resident)

  case class Possible(residents: Seq[Resident], drinks: Seq[Drink], colours: Seq[Colour], smokes: Seq[Smoke], pets: Seq[Pet])

  lazy val solve: Solution = {
    val solution = possibleSolutions.next() // get first (and possible only) solution
    Solution(
      waterDrinker = solution.residents(solution.drinks.indexOf(Water)),
      zebraOwner = solution.residents(solution.pets.indexOf(Zebra)))
  }

  //#10 The Norwegian lives in the first house.
  def validResident(residents: Seq[Resident]) =
    residents.indexOf(Norwegian) == 0

  //#09 Milk is drunk in the middle house.
  //#05 The Ukrainian drinks tea.
  def validResidentDrink(residents: Seq[Resident], drinks: Seq[Drink]) =
    drinks.indexOf(Milk) == 2 &&
      drinks.indexOf(Tea) == residents.indexOf(Ukrainian)

  //#02 The Englishman lives in the red house.
  //#04 Coffee is drunk in the green house.
  //#06 The green house is immediately to the right of the ivory house.
  //#15 The Norwegian lives next to the blue house.
  def validResidentDrinkColour(residents: Seq[Resident], drinks: Seq[Drink], colours: Seq[Colour]) =
    colours.indexOf(Red) == residents.indexOf(Englishman) &&
      colours.indexOf(Green) == drinks.indexOf(Coffee) &&
      (colours.indexOf(Green) - colours.indexOf(Ivory) == 1) &&
      (Math.abs(colours.indexOf(Blue) - residents.indexOf(Norwegian)) == 1)

  //#08 Kools are smoked in the yellow house.
  //#13 The Lucky Strike smoker drinks orange juice.
  //#14 The Japanese smokes Parliaments.
  def validNoPet(residents: Seq[Resident], drinks: Seq[Drink], colours: Seq[Colour], smokes: Seq[Smoke]) =
    smokes.indexOf(Kools) == colours.indexOf(Yellow) &&
      smokes.indexOf(Lucky) == drinks.indexOf(Orange) &&
      smokes.indexOf(Parliaments) == residents.indexOf(Japanese)

  //#03 The Spaniard owns the dog.
  //#07 The Old Gold smoker owns snails.
  //#11 The man who smokes Chesterfields lives in the house next to the man with the fox.
  //#12 Kools are smoked in the house next to the house where the horse is kept.
  def validAll(residents: Seq[Resident], drinks: Seq[Drink], colours: Seq[Colour], smokes: Seq[Smoke], pets: Seq[Pet]) =
    pets.indexOf(Dog) == residents.indexOf(Spaniard) &&
      pets.indexOf(Snail) == smokes.indexOf(OldGold) &&
      (Math.abs(pets.indexOf(Fox) - smokes.indexOf(Chesterfields)) == 1) &&
      (Math.abs(pets.indexOf(Horse) - smokes.indexOf(Kools)) == 1)

  lazy val possibleSolutions: Iterator[Possible] = for {
    residents <- Seq(Englishman, Spaniard, Ukrainian, Norwegian, Japanese).permutations if (validResident(residents))
    drinks <- Seq(Water, Milk, Coffee, Tea, Orange).permutations if (validResidentDrink(residents, drinks))
    colours <- Seq(Red, Green, Blue, Yellow, Ivory).permutations if (validResidentDrinkColour(residents, drinks, colours))
    smokes <- Seq(OldGold, Kools, Lucky, Chesterfields, Parliaments).permutations if (validNoPet(residents, drinks, colours, smokes))
    pets <- Seq(Dog, Fox, Horse, Snail, Zebra).permutations if (validAll(residents, drinks, colours, smokes, pets))
  } yield Possible(residents, drinks, colours, smokes, pets)

}