import scala.util.Random

class Robot {
  private var myName = create() 
  private def create() = Random.alphanumeric.filter(c => c>='A' && c<='Z').take(2).mkString +
                                          Random.alphanumeric.filter(c => c>='0' && c<='9').take(3).mkString

  def name() = myName
  def reset() = myName = create()  
}