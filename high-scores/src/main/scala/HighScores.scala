object HighScores {

  def personalBest(scores: List[Int]): Int = scores.max

  def latest(scores: List[Int]): Int = scores.last

  def personalTop(scores: List[Int]): List[Int] =
    scores.sorted(Ordering.Int.reverse).take(3)

  def report(scores: List[Int]): String = (scores.max, scores.last) match {
    case (a, b) if (a > b) =>
      s"Your latest score was $b. That's ${a - b} short of your personal best!"

    case (a, b) =>
      s"Your latest score was $b. That's your personal best!"
  }

}