import ForthError.ForthError

class Forth extends ForthEvaluator {
  def eval(text: String): Either[ForthError, ForthEvaluatorState] = (new MyState(text)).getRightOrError
}

class MyState(exp: String) extends ForthEvaluatorState {
  type LStack = List[String]
  private val number = "([0-9]+)"r

  // expandable expressions from 1 to many (e.g. : foo 1 2 swap ; foo -> "1", "2", "swap")
  var subs: Map[String, LStack] = Map()

  // internal representation of stack as List
  var internalState = parse(exp.toLowerCase().split(" ").toList, List())

  // expands the head of the expression being parsed with the dictionary we have so far
  def expandExpr(stack: LStack): LStack = stack match {
    case h :: tail if (subs.keySet.contains(h)) => (subs.get(h).get ++ tail)
    case _ => stack
  }

  // expands all the expressions, so that they are greedy when creating a substitution
  // this allows for ": foo 10 ; : foo foo 1 + ; foo" to be "11" instead of infinite recursive
  def expandAllExpr(params: LStack): LStack = params match {
    case h :: tail if (subs.keySet.contains(h)) => expandAllExpr(subs.get(h).get ++ tail)
    case h :: tail => h :: expandAllExpr(tail)
    case _ => params
  }

  // check substitution is not a number; adds it to the dictionary of substitutions, expanding all terms
  def addSubstitution(cmd: String, exp: LStack, stack: LStack): Either[ForthError, LStack] = {
    if (number.findFirstIn(cmd).isDefined)
      Left(ForthError.InvalidWord)
    else {
      val (params, res) = exp.span(_ != ";") // pick everything before the ";"
      subs = subs + (cmd -> expandAllExpr(params))
      parse(expandExpr(res.tail), stack) // take everything after the ";"
    }
  }

  // consumes the input Expression, creating the stack, and updating the dictionary
  def parse(exp: LStack, stack: LStack): Either[ForthError, LStack] =
    (exp.headOption.getOrElse(""), stack) match {
      case ("", stk) => Right(stk)
      case ("+", a :: b :: stk) => parse(expandExpr(exp.tail), (a.toInt + b.toInt).toString() +: stk)
      case ("-", a :: b :: stk) => parse(expandExpr(exp.tail), (b.toInt - a.toInt).toString() +: stk)
      case ("*", a :: b :: stk) => parse(expandExpr(exp.tail), (a.toInt * b.toInt).toString() +: stk)
      case ("/", "0" :: b :: stk) => Left(ForthError.DivisionByZero)
      case ("/", a :: b :: stk) => parse(expandExpr(exp.tail), (b.toInt / a.toInt).toString() +: stk)
      case ("swap", a :: b :: stk) => parse(expandExpr(exp.tail), b :: a :: stk)
      case ("dup", a :: stk) => parse(expandExpr(exp.tail), a :: a :: stk)
      case ("drop", a :: stk) => parse(expandExpr(exp.tail), stk)
      case ("over", a :: b :: stk) => parse(expandExpr(exp.tail), b :: a :: b :: stk)
      case (number(n), stk) => parse(expandExpr(exp.tail), n +: stk)
      case (":", stk) if (exp.size > 2) => addSubstitution(exp.tail.head, exp.tail.tail, stack)
      case _ => Left(ForthError.UnknownWord)
    }

  def getRightOrError: Either[ForthError, ForthEvaluatorState] = internalState match {
    case Left(error) => Left(error)
    case Right(list) => Right(this)
  }

  override def toString: String = internalState match {
    case Left(error) => error.toString()
    case Right(list) => list.reverse.mkString(" ")
  }

}