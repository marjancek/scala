object NthPrime {
  def prime(nth: Int): Option[Int] = {
    lazy val primes: Stream[Int] = 2 #:: Stream.from(3, 2)
      .filter(n => primes.takeWhile(p => p * p <= n).forall(p => n % p > 0))

    if (nth == 0) None
    else Some(primes.take(nth).last)
  }
}