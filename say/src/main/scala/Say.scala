object Say {

  val units = Map(0 -> "", 1 -> "one", 2 -> "two", 3 -> "three", 4 -> "four", 5 -> "five", 6 -> "six", 7 -> "seven", 8 -> "eight", 9 -> "nine")
  val teens = Map(0 -> "ten", 1 -> "eleven", 2 -> "twelf", 3 -> "thirteen", 4 -> "fourteen", 5 -> "fifteen", 6 -> "sixteen", 7 -> "seventeen", 8 -> "eighteen", 9 -> "nineteen")
  val tens = Map(2 -> "twenty", 3 -> "thirty", 4 -> "forty", 5 -> "fifty", 6 -> "sixty", 7 -> "seventy", 8 -> "eighty", 9 -> "ninety")
  val pow3 = Map(0 -> "", 1 -> " thousand", 2 -> " million", 3 -> " billion", 4 -> " trillion", 4 -> " quadrillion", 4 -> " quintillion")

  def inEnglish(n: Long): Option[String] = {
    // handle special cases for zero, and out of boundaries
    if (n >= 1000000000000l || n < 0)
      None
    else if (n == 0)
      Some("zero")
    else
      // 1234567890 becomes 098, 765, 432, 1 , we then translate that
      Some(resolve(n.toString().reverse.grouped(3).toList))
  }

  def resolve(g: List[String]): String =
    // first we pair with pow2 -> (098, ""), 765, "thousand"), (432, "million"), (1, "billion")
    (for (t <- g.zipWithIndex) yield (t._1, pow3(t._2)))
      // then we transform each to text (..., ..., "two hundred thirty-four million", ...)
      .map({ case (n, b) => if (n.toInt > 0) toWords(n) + b else "" })
      // filter out anything empty, reverse it back, and glue everything
      .filter(_.length() > 0)
      .reverse
      .mkString(" ")

  // transform unit-tens-hundreds triad into text
  def toWords(udh: String): String = {
    val h = if (udh.length() > 2) udh.charAt(2) - '0' else 0
    val d = if (udh.length() > 1) udh.charAt(1) - '0' else 0
    val u = if (udh.length() > 0) udh.charAt(0) - '0' else 0
    // create text for the hundreds
    val hundreds = if (h > 0) units(h) + " hundred" else ""

    // Teens are special. Also, we need a hyphen between tens and units
    val du = (u, d) match {
      case (u, 0) => units(u)
      case (u, 1) => teens(u)
      case (0, d) => tens(d)
      case (u, d) => tens(d) + "-" + units(u)
    }

    // If we have both hundreds and other, we need a space in between
    if (hundreds.length() > 0 && du.length() > 0)
      hundreds + " " + du
    else
      hundreds + du
  }
}