import scala.util.matching.Regex
object PigLatin {
  val rule1 = "(a|e|i|o|u|xr|yt)(.*)"r
  val rule2 = "([^aiueo]+)(.*)"r
  val rule3 = "([^aiueo]?qu)(.*)"r
  val rule4 = "([^aiueo]+)(y.*)"r
  val AY = "ay"

  def translate(phrase: String): String = 
    phrase.split(" ").map(translateWord).mkString(" ")

  def translateWord(word: String): String = word match {
    case rule1(vowel, rest) => word + AY
    case rule4(start, yRest) => yRest + start + AY
    case rule3(start, quRest) => quRest + start + AY
    case rule2(consonants, rest) => rest + consonants + AY
    case _ => word
  }

}