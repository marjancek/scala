object DifferenceOfSquares {

  def sumOfSquares(n: Int): Int = 1.to(n).map(pow2).sum

  def squareOfSum(n: Int): Int = pow2(1.to(n).sum)

  def differenceOfSquares(n: Int): Int = squareOfSum(n) - sumOfSquares(n)

  private def pow2(n: Int): Int = n * n
}
